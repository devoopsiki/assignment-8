export { default as TheHeader } from './TheHeader.vue';
export { default as BuyModal } from './BuyModal.vue';
export { default as SuccessModal } from './SuccessModal.vue';