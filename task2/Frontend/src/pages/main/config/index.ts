import type { Borscht } from "../types";
 
export const borschts = [
  {
    idx: 0,
    title: 'Вегетарианский борщ😞',
    imgName: 'vegetarian',
    price: 1000,
    initialAmount: 50,
    amount: 50
  },
  {
    idx: 1,
    title: 'Зеленый борщ✅',
    imgName: 'green',
    price: 1100,
    initialAmount: 50,
    amount: 50
  },
  {
    idx: 2,
    title: 'Красный борщ😡',
    imgName: 'red',
    price: 1200,
    initialAmount: 50,
    amount: 50
  },
  {
    idx: 3,
    title: 'Белый борщ😳',
    imgName: 'white',
    price: 1300,
    initialAmount: 50,
    amount: 50
  },
  {
    idx: 4,
    title: 'Борщ с фасолью😶',
    imgName: 'with-beans',
    price: 1400,
    initialAmount: 50,
    amount: 50
  },
  {
    idx: 5,
    title: 'Борщ для космонавтов😎',
    imgName: 'astronaut',
    price: 1000,
    initialAmount: 50,
    amount: 50
  },
] as Borscht[];
