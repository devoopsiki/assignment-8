export type Borscht = {
  idx: number;
  title: string;
  imgName: string;
  price: number;
  initialAmount: number;
  amount: number;
};

export type BasketBorscht = Borscht & {
  addedAmount: number;
  totalPrice: number;
};