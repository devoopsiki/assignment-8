import { createRouter, createWebHistory } from 'vue-router';
import type { RouteRecordRaw } from 'vue-router';

import MainPage from '@/pages/main/MainPage.vue';

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'MainPage',
    component: MainPage
  }
];

const base = '/';

const router = createRouter({
  history: createWebHistory(base),
  routes
});

export { router };
