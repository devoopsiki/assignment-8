import { h, resolveComponent, createApp } from 'vue';

import { router } from './router';

import '@/assets/css/main.css';

const App = {
  name: 'AviaApp',
  render: () => h(resolveComponent('router-view'))
};

const app = createApp(App);

app
  .use(router)
  .mount('#app');

console.log('PORT: ', import.meta.env.VITE_PORT);