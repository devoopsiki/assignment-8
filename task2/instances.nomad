variable "image_name" {
  type = string
}

job "multi-service-app" {
  datacenters = ["dc1"]
  type        = "service"

  group "web" {
    count = 3

    network {
      port "web" {}
    }

    task "kek" {
      driver = "docker"

      config {
        image = "${var.image_name}"
        ports = ["web"]
      }

      env { 
        VITE_PORT = "${NOMAD_HOST_PORT_web}"
      }
    }
  }
}
